/*

  This file depends on having an S3 bucket and cloudfront distribution setup.

  These tutorials were followed:
    - http://kynatro.com/blog/2018/01/03/a-step-by-step-guide-to-creating-a-password-protected-s3-bucket/
    - https://hackernoon.com/serverless-password-protecting-a-static-website-in-an-aws-s3-bucket-bfaaa01b8666

    The second has a fix for possible "AccessDenied" issues.

    AWS troubleshooting docs are very informative: https://aws.amazon.com/premiumsupport/knowledge-center/s3-rest-api-cloudfront-error-403/
*/

const s3 = require('s3');

var client = s3.createClient({
  s3Options: {
    accessKeyId: process.env.s3AccessKeyId,
    secretAccessKey: process.env.s3SecretAccessKey,
    region: process.env.s3Region,
    // endpoint: 's3.yourdomain.com',
    // sslEnabled: false
    // any other options are passed to new AWS.S3()
    // See: http://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/Config.html#constructor-property
  },
});

const appParams = {
  localDir: `./out`,
  deleteRemoved: true,

  s3Params: {
    Bucket: process.env.s3BucketName,
    Prefix: `${process.env.BITBUCKET_BRANCH}/app/`,
    // other options supported by putObject, except Body and ContentLength.
    // See: http://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/S3.html#putObject-property
  },
};

const appUploader = client.uploadDir(appParams);
appUploader.on('error', function(err) {
  console.error("app unable to sync:", err.stack);
});
appUploader.on('progress', function() {
  console.log("app progress", appUploader.progressAmount, appUploader.progressTotal);
});
appUploader.on('end', function() {
  console.log("app done uploading");
});


const nextParams = {
  localDir: `./_next`,
  deleteRemoved: true,

  s3Params: {
    Bucket: process.env.s3BucketName,
    Prefix: `${process.env.BITBUCKET_BRANCH}/app/`,
    // other options supported by putObject, except Body and ContentLength.
    // See: http://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/S3.html#putObject-property
  },
};

const nextUploader = client.uploadDir(nextParams);
appUploader.on('error', function(err) {
  console.error("_next unable to sync:", err.stack);
});
nextUploader.on('progress', function() {
  console.log("_next progress", nextUploader.progressAmount, nextUploader.progressTotal);
});
nextUploader.on('end', function() {
  console.log("_next done uploading");
});


const storybookParams = {
  localDir: `./out-storybook`,
  deleteRemoved: true,

  s3Params: {
    Bucket: process.env.s3BucketName,
    Prefix: `${process.env.BITBUCKET_BRANCH}/storybook/`,
    // other options supported by putObject, except Body and ContentLength.
    // See: http://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/S3.html#putObject-property
  },
};

const storybookUploader = client.uploadDir(storybookParams);
storybookUploader.on('error', function(err) {
  console.error("storybook unable to sync:", err.stack);
});
storybookUploader.on('progress', function() {
  console.log("storybook progress", storybookUploader.progressAmount, storybookUploader.progressTotal);
});
storybookUploader.on('end', function() {
  console.log("storybook done uploading");
});