import React, { Component } from 'react';

import Router from 'next/router';
import styled from 'styled-components';
import { Controller, Scene } from 'react-scrollmagic';
import { Tween, Timeline } from 'react-gsap';
import intl from "react-intl-universal";
import axios from 'axios';

import Header from '../components/header';
import Footer from '../components/footer';
import HeroSection from '../components/hero';
import SequenceOne from '../components/sequence-one';
import SequenceTwo from '../components/sequence-two';


const locale = {~ localeString ~};
const supportedLocales = {~ supportedLocalesString ~};




const PageWrapper = styled.div`
  position: relative;
`;

const HeaderWrapper = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  z-index: 1000;
`

class App extends Component {

  static async getInitialProps(ctx) {
    console.log('pathname', ctx.pathname);
    // const response = await axios.get(`/static/locales/${currentLocale}.json`);
    // // init method will load CLDR locale data according to currentLocale
    // await intl.init({
    //   currentLocale,
    //   locales: {
    //     [currentLocale]: response.data
    //   }
    // });

    return {};
  }

  constructor() {
    super();

      this.changeTranslation = this.changeTranslation.bind(this);
  }

  changeTranslation(event) {
    let pageName = '';
    if(event.target.value !== 'en-us') {
      pageName = event.target.value;
    }
    Router.push(`/${pageName}`);
  }

  render() {
    return (
      <PageWrapper>

        {/* We should be using this sparingly. It should mostly be for overrides of existing browser styles. */}
        <style jsx global>{`
          body {
            margin: auto
          }
        `}</style>

        <HeaderWrapper>
          <Header />
          <select onChange={this.changeTranslation}>
            {supportedLocales.map(_locale =>
              <option
                value={_locale.code}
                selected={_locale.code === locale.code}
                key={_locale.code}
              >{_locale.name}</option>
            )}
          </select>
        </HeaderWrapper>

        <HeroSection />

        <SequenceOne />

        <SequenceTwo />

        <Footer />

      </PageWrapper>
    );
  }
}

export default App;
