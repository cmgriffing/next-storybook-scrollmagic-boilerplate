'use strist';

const fs = require('fs');
const mustache = require('mustache');
const path = require('path');

const supportedLocales = [
  {
    name: 'English (US)',
    code: 'en-us'
  }, {
    name: 'English (UK)',
    code: 'en-uk'
  }, {
    name: 'Welsh',
    code: 'cy'
  },
];

const translationPath = path.resolve(__dirname, './translation.tpl.js');
const template = fs.readFileSync(translationPath, {encoding: 'utf-8'});

mustache.escape = function(text) {return text;};
mustache.parse(template);



supportedLocales.map(locale => {
  const rendered = mustache.render(
    template,
    {
      localeString: JSON.stringify(locale),
      supportedLocalesString: JSON.stringify(supportedLocales),
    },
    {},
    // use custom tags to prevent collisions with jsx
    [ '{~', '~}']
  );

  let pageName = 'index';
  if(locale.code !== 'en-us') {
    pageName = locale.code;
  }

  fs.writeFileSync(path.resolve(__dirname, `../pages/${pageName}.js`), rendered);
});
