import styled from 'styled-components';

export const PinWrapper = styled.div`
  height: 100vh;
  width: 100vw;
  overflow: hidden;
`;

export const Panel = styled.section`
  height: 100vh;
  width: 100vw;
  background: ${ props => props.color || '#BBB' };
  display: flex;
  align-items: center;
  justify-content: center;
  position: absolute;
`;