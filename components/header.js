import React from 'react';
import styled from 'styled-components';

const HeaderWrapper = styled.div`
  overflow: hidden;
  height: 30px;
`;

const TopHeader = styled.div`
  background: yellow;
  height: 30px;
  transform: translateY(${props => props.scrolled ? '-100%' : '0%'});
  transition: transform 0.242s;
`;

const BottomHeader = styled.div`
  background: orange;
  height: 30px;
  transform: translateY(${props => props.scrolled ? '-100%' : '0%'});
  transition: transform 0.242s;
`;

class Header extends React.Component {

  constructor() {
    super();
    this.handleScroll = this.handleScroll.bind(this);

    this.state = {
      scrolled: false
    }
  }

  componentDidMount() {
    window.addEventListener('scroll', this.handleScroll);
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll);
  }

  handleScroll(event) {
    const { scrolled } = this.state;
    if(window.scrollY > 30 && !scrolled) {
      this.setState({
        scrolled: true
      })
    } else if(window.scrollY <= 30 && scrolled) {
      this.setState({
        scrolled: false
      });
    }
  }

  render() {

    const { scrolled } = this.state;

    return (
      <HeaderWrapper>
        <TopHeader scrolled={scrolled} />
        <BottomHeader scrolled={scrolled} />
      </HeaderWrapper>
    );
  }
}

export default Header;
