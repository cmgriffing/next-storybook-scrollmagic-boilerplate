import { Component } from 'react';
import styled from 'styled-components';

import { Panel } from './styled/common';

const HeroSequenceWrapper = styled.div`
  height: 100vh;
  position: relative;
`;

export default class HeroSection extends Component {

  render() {
    return (

      <HeroSequenceWrapper>
        <Panel id="temp" color="tan" >
          <span>Something</span>
        </Panel>
      </HeroSequenceWrapper>
    )
  }

}