import { Component } from 'react';

import styled from 'styled-components';
import { Controller, Scene } from 'react-scrollmagic';
import { Tween, Timeline } from 'react-gsap';

import { Panel, PinWrapper } from './styled/common';

// The amount of Panels determines the the height
const SequenceOneWrapper = styled.div`
  height: 600vh;
  position: relative;
`;

export default class SequenceOne extends Component {
  render(){
    return (
      <SequenceOneWrapper>
        <Controller>

          {/* TODO: I don't like how this has to listen for the temp trigger element by id as its target. Not sure how to refactor */}
          <Scene
            triggerHook="onLeave"
            duration="500%"
            pin
          >
            <Timeline
              wrapper={<PinWrapper />}
            >
              <Panel color="brown">
                <span>Something</span>
              </Panel>
              <Tween
                from={{ y: '100%' }}
                to={{ y: '0%' }}
              >
                <Panel>
                  <span>1</span>
                </Panel>
              </Tween>
              <Tween
                from={{ y: '100%' }}
                to={{ y: '0%' }}
              >
                <Panel color="blue">
                  <span>2</span>
                </Panel>
              </Tween>

              <Tween
                from={{ x: '100%' }}
                to={{ x: '0%' }}
              >
                <Panel color="red">
                  <span>3</span>
                </Panel>
              </Tween>

              <Tween
                from={{ x: '100%' }}
                to={{ x: '0%' }}
              >
                <Panel color="yellow">
                  <span>4</span>
                </Panel>
              </Tween>
              <Tween
                from={{ x: '100%' }}
                to={{ x: '0%' }}
              >
                <Panel color="brown">
                  <span>5</span>
                </Panel>
              </Tween>
              <Tween
                from={{ y: '100%' }}
                to={{ y: '0%' }}
              >
                <Panel>
                  <span>1</span>
                </Panel>
              </Tween>
              <Tween
                from={{ y: '100%' }}
                to={{ y: '0%' }}
              >
                <Panel color="red">
                  <span>2</span>
                </Panel>
              </Tween>
            </Timeline>
          </Scene>
        </Controller>
      </SequenceOneWrapper>
    );
  }
}