import { Component } from 'react';

import styled from 'styled-components';
import { Controller, Scene } from 'react-scrollmagic';
import { Tween, Timeline } from 'react-gsap';

import { Panel, PinWrapper } from './styled/common';

// The amount of Panels determines the the height
const SequenceTwoWrapper = styled.div`
  height: 200vh;
  position: relative;
`;

export default class SequenceTwo extends Component {
  render(){
    return (
      <SequenceTwoWrapper>
        <Controller>
          <Scene
            triggerHook="onLeave"
            duration="100%"
            pin
          >
            <Timeline
              wrapper={<PinWrapper />}
            >
              <Panel id="temp" color="tan" >
                <span>Something</span>
              </Panel>
              <Tween
                from={{ y: '100%' }}
                to={{ y: '0%' }}
              >
                <Panel color="red">
                  <span>2</span>
                </Panel>
              </Tween>
            </Timeline>
          </Scene>
        </Controller>
      </SequenceTwoWrapper>
    );
  }
}